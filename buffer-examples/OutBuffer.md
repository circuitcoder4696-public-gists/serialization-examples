# Out-buffer examples:

Origional-example by rikki_cattermole#6626:
```d
import std;
void main()
{
    Buffer buffer = Buffer([1, 2, 3, 4, 5, 6, 7, 8]);
    UserType[2] ut;
    buffer.readArray(ut[]);
    
    writeln(ut);
}

struct UserType {
    int v;
}

struct Buffer {
    ubyte[] data;
    
    void readArray(T)(T[] slice...) {
        import std.traits : FieldNameTuple;
        
        foreach(ref output; slice) {
            static foreach(M; FieldNameTuple!T) {
                {
                     auto m = &__traits(getMember, output, M);
                    alias Field = typeof(*m);
                    
                    if (this.data.length >= Field.sizeof) {
                        *m = *cast(Field*)data.ptr;
                        data = data[Field.sizeof .. $];
                    }                     
                }
            }
        }
    }
}
```

The updated version:
```d
import std;
void main()
{
    Buffer buffer = Buffer([1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8]);
    UserType[2] ut;
    UserType ut1;
    int[] d00= new int[3];
    buffer.readArray(ut[]);
    buffer.readValue(ut1);
    buffer.readArray(d00);
    writeln(ut);
    writeln(ut1);
    writeln(d00);
}

struct UserType {
    int v;
}

struct Buffer {
    ubyte[] data;
    
    void readArray(T)(T[] slice...) {
        import std.traits : FieldNameTuple;
        static if(is(T == struct)) {
            foreach(ref output; slice) {
                writeln(FieldNameTuple!T);
                static foreach(M; FieldNameTuple!T) {{
                    auto m = &__traits(getMember, output, M);
                    alias Field = typeof(*m);
                    if (this.data.length >= Field.sizeof) {
                        *m = *cast(Field*)data.ptr;
                        data = data[Field.sizeof .. $];
                    }
                }}
            }
        }else{
            size_t size= slice.length * T.sizeof;
            T* m= slice.ptr;
            // alias T= typeof(*m);
            if (this.data.length >= size) {
                m[0 .. slice.length]= cast(T[]) data[0 .. size];
                // data= data[T.sizeof .. $];
            }
        }
    }
    void readValue(T)(ref T value) {
        import std.traits : FieldNameTuple;
        
        // foreach(ref output; slice) {
            static foreach(M; FieldNameTuple!T) {
                {
                    auto m = &__traits(getMember, value, M);
                    alias Field = typeof(*m);
                    
                    if (this.data.length >= Field.sizeof) {
                        *m = *cast(Field*)data.ptr;
                        data = data[Field.sizeof .. $];
                    }                     
                }
            }
        // }
    }
}
```

```
v
v
[UserType(67305985), UserType(134678021)]
UserType(67305985)
[134678021, 67305985, 134678021]
```